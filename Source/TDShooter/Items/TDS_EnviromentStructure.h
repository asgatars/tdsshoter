// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_IGameActor.h"
#include "TDShooter/Character/TDS_StateEfect.h"
#include "TDS_EnviromentStructure.generated.h"

UCLASS()
class TDSHOOTER_API ATDS_EnviromentStructure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_EnviromentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	virtual EPhysicalSurface GetSurfaceType() override;


	//Effect
	TArray<UTDS_StateEfect*> Effects;

	//Interface
	virtual TArray<UTDS_StateEfect*> GetAllCurentEffects() override;
	virtual void RemoveEffect(UTDS_StateEfect* RemoveEffect) override;
	virtual void AddEffect(UTDS_StateEfect* newEffect) override;
	//Interface End
};
