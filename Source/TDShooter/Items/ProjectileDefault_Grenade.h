// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TDSHOOTER_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	//Called When the game Starts or when Spawned
	virtual void BeginPlay() override;

public:
	//Called Every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose (float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitCom, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                                    FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	void Explose();

	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;
	float TimeToExpolse = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
	
	
};
