// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"


#include "DrawDebugHelpers.h"
#include "DroppableItemDefault.h"
#include "TDS_IGameActor.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDShooter/Character/TDSInventoryComponent.h"
#include "TDShooter/Character/TDS_StateEfect.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);

	MagDropLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Magazine Drop Location"));
	MagDropLocation->SetupAttachment(RootComponent);

	SleevBulletDropLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Sleev Bullet Drop Location"));
	SleevBulletDropLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	WeaponInit();
}



// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if(GetWeaponRound() >0)
	{
		if(WeaponFiring)
			if(FireTimer < 0.f)
			{
				if(!WeaponReloading)
				Fire();				
			}
			else
			{
				FireTimer -= DeltaTime;
			}
	}

}
			
	

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if(WeaponReloading)
	{
		if(ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
	
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if(!WeaponReloading)
	{
		if(WeaponFiring)
		{
			if(ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
			
		}

		if(CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if(CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}		
	}
	if(ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. Min = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);	
}


void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent((true));
	}

}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = false;	
	//UE_LOG(LogTemp, Error, TEXT("AWeaponDefalt::CheckCanWeaponReload Owner = %s"), *GetOwner()->GetName());
	
	if(GetOwner())
	{		
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		
		if(MyInv)
		{
			
			int8 AvelibleAmmoForWeapon;
			if(MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvelibleAmmoForWeapon))
			{
				result = true;
			}
		}
	}
	
	return result;
}

void AWeaponDefault::SetWeaponStateFire(bool bIsAttacked)
{
	if(CheckWeaponCanFire())
		WeaponFiring = bIsAttacked;
	else
		WeaponFiring = false;
		FireTimer = 0.01f;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

int8 AWeaponDefault::GetAvelibleAmmoForReload()
{
	int8 AvelibleAmmoForWeapon = WeaponSetting.MaxRound;
	
	if(GetOwner())
	{
		
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if(MyInv)
		{
			
			if(MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvelibleAmmoForWeapon))
			{
				AvelibleAmmoForWeapon =  AvelibleAmmoForWeapon;
			}
		}
	}
	return AvelibleAmmoForWeapon;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	UAnimMontage* AnimToPlay = nullptr;

	//ToDo  if Weapon Aiming

	
		if(WeaponAiming)
		AnimToPlay = WeaponSetting.AnimCharFireAim;
		else
		AnimToPlay = WeaponSetting.AnimCharFire;

	if (WeaponSetting.AnimCharFire
        && SkeletalMeshWeapon
        && SkeletalMeshWeapon->GetAnimInstance())//Bad Code? maybe best way init local variable or in func
        	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.AnimCharFire);
        	}
	
/*
	if (WeaponSetting.ShellBullets.DropMesh)
	{
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
		{
			InitDropMesh(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset, WeaponSetting.ShellBullets.DropMeshImpulseDir, WeaponSetting.ShellBullets.DropMeshLifeTime, WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpulse, WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}	
	}
*/
	
	
	FireTimer = WeaponSetting.RateOfFire;
	WeaponInfo.Round = WeaponInfo.Round - 1;
	ChangeDispersionByShoot();

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentLocation());
	
	OnWeaponFireStart.Broadcast(AnimToPlay);
	
	int8 NumberProjectile = GetNumberProjectileByShoot();


	
	if(ShootLocation)
	{		
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for(int8 i = 0; i< NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();
			
		
			if(ProjectileInfo.Projectile)
			{
				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();

				FMatrix myMtrix(Dir, FVector(0.0f, 0.0f, 0.0f), FVector(0.0f, 0.0f, 0.0f), FVector::ZeroVector);
				SpawnRotation = myMtrix.Rotator();
				
				//Projectile Init ballistic fire
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if(myProjectile)
				{
				
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
					
				}
				FVector SpawnSBLocation = SleevBulletDropLocation->GetComponentLocation();
				FRotator SpawnSBRotation = SleevBulletDropLocation->GetRelativeRotation();
				FWeaponInfo myDropItemInfo = WeaponSetting;
				FActorSpawnParameters SBSpawnParam;
				SBSpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SBSpawnParam.Owner = GetOwner();
				SBSpawnParam.Instigator = GetInstigator();
				ADroppableItemDefault* myDropItem = Cast<ADroppableItemDefault>(GetWorld()->SpawnActor(myDropItemInfo.SleevBullet, & SpawnSBLocation, & SpawnSBRotation, SBSpawnParam));
				
			}
			else
			{
				//ToDo Projectile null Init trace fire
				FHitResult HitResult;
				FVector Start = ShootLocation->GetComponentLocation();
				FCollisionQueryParams TraceParam;
				TraceParam.bDebugQuery = true;
				GetWorld()->LineTraceSingleByChannel(HitResult, Start, ShootEndLocation,ECC_Visibility, TraceParam);
				EPhysicalSurface mySurfaceType = HitResult.PhysMaterial->SurfaceType;	
				if(HitResult.bBlockingHit)
				{
					UE_LOG(LogTemp, Warning, TEXT("Line Trace hit somthing"))	
				}
				else
					UE_LOG(LogTemp, Warning, TEXT("Line Trace not hit somthing"))

				
				ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(HitResult.GetActor());
				if(myInterface)
				{
					UTypes::AddEffectBySurfaceType(HitResult.GetActor(), ProjectileInfo.Effect, mySurfaceType);					
				}

				// if(HitResult.GetActor()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass())) //ITDS_IGameActor::UClassType::StaticClass()
				// {
				// 	ITDS_IGameActor::Execute_AviableForEffects(HitResult.GetActor());
				// 	ITDS_IGameActor::Execute_AviableForEffectBP(HitResult.GetActor());
				// }

				UGameplayStatics::ApplyPointDamage(HitResult.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, HitResult.TraceStart, HitResult, GetInstigatorController(), this, NULL);
			}
			
		}		
		
	}

	if(GetWeaponRound() <= 0 && !WeaponReloading)
	{
		//Init Reload
		if(CheckCanWeaponReload())
			InitReload();
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	//ToDo Dispersion
	BlockFire = false;
	switch (NewMovementState)
	{
		case EMovementState::Aim_State:
			WeaponAiming = true;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
			break;

		case EMovementState::AimWalk_State:
			WeaponAiming = true;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionReduction;
			break;

		case EMovementState::Walk_State:
			WeaponAiming = false;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
			break;

		case EMovementState::Run_State:
			WeaponAiming = false;
			CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionMax;
			CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionMin;
			CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.RunDispersionRecoil;
			CurrentDispersionReduction = WeaponSetting.DispersionWeapon.RunDispersionReduction;
			break;

		case EMovementState::Sprint_State:
			WeaponAiming = false;
			BlockFire = true;
			SetWeaponStateFire(false); //settrigger to false
			//BlockFire
			break;

		default:
			break;
	}
	
}

void AWeaponDefault::ChangeDispersionByShoot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{	
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector::ZeroVector;

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	if(tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
		{
			EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
			if(ShowDebug)
				DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 0.1f, (uint8)'\000', 1.0f);
		}
	else
		{
			EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
			if(ShowDebug)
				DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(),  ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 0.1f, (uint8)'\000', 1.0f);
		}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}
	

	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShoot() const
{
	return WeaponSetting.NumberProjectileByShoot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;
	//ToDo Reload Anim
	if(WeaponSetting.AnimCharReload)
	OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload);

	//When Reload Spawn empty magazine
	FVector SpawnLocation = MagDropLocation->GetComponentLocation();
	FRotator SpawnRotation = MagDropLocation->GetRelativeRotation();
	FWeaponInfo myDropMagInfo = WeaponSetting;
	FActorSpawnParameters SpawnParam;
	SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParam.Owner = GetOwner();
	SpawnParam.Instigator = GetInstigator();
	ADroppableItemDefault* myDropItem = Cast<ADroppableItemDefault>(GetWorld()->SpawnActor(myDropMagInfo.MagazineDrop, & SpawnLocation, & SpawnRotation, SpawnParam));
	
	

}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	
	int8 AvelibleAmmoFromInventory = GetAvelibleAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - WeaponInfo.Round;

	if(NeedToReload > AvelibleAmmoFromInventory)
	{
		WeaponInfo.Round += AvelibleAmmoFromInventory;
		AmmoNeedTakeFromInv = AvelibleAmmoFromInventory;
	}
	else
	{
		WeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if(SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
		//Flag for Drop item (Magazine and sleev Bullet)
		//DropClipFlag = false;
}
