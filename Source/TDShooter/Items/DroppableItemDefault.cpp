// Fill out your copyright notice in the Description page of Project Settings.


#include "DroppableItemDefault.h"
#include "Niagara/Public/NiagaraComponent.h"
//#include "IDetailTreeNode.h"
#include "NiagaraFunctionLibrary.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
ADroppableItemDefault::ADroppableItemDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Item Mesh"));
	ItemMesh->SetupAttachment(RootComponent);
	ItemMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	ItemMesh->SetSimulatePhysics(true);
	ItemMesh->SetCanEverAffectNavigation(false);

	//VisualSelection = CreateDefaultSubobject<UNiagaraSystem>("Niagara System");
	

	
}

// Called when the game starts or when spawned
void ADroppableItemDefault::BeginPlay()
{
	Super::BeginPlay();
	LifeTime = 5.0f;

	if(VisualSelection)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), VisualSelection, GetActorLocation());
	}
}

// Called every frame
void ADroppableItemDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	LifeTick(DeltaTime);
}

void ADroppableItemDefault::LifeTick(float DeltaTime)
{
	
		if(LifeTime < 0.0f)
		{
			Destroy();
		}
		else
		{
			LifeTime -= DeltaTime;
		}

}

