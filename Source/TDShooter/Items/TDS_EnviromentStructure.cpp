// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnviromentStructure.h"
#include "Materials/MaterialInstance.h"
#include "PhysicalMaterials/PhysicalMaterial.h"


// Sets default values
ATDS_EnviromentStructure::ATDS_EnviromentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDS_EnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_EnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<UTDS_StateEfect*> ATDS_EnviromentStructure::GetAllCurentEffects()
{


	return Effects;
}

void ATDS_EnviromentStructure::RemoveEffect(UTDS_StateEfect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDS_EnviromentStructure::AddEffect(UTDS_StateEfect* newEffect)
{
	Effects.Add(newEffect);
}

EPhysicalSurface ATDS_EnviromentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if(myMesh)
	{
		UMaterialInterface* myMeaterial =  myMesh->GetMaterial(0);
		if(myMeaterial)
		{
			Result = myMeaterial->GetPhysicalMaterial()->SurfaceType;
		}
		
	}

	return Result;
}



