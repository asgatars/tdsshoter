// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExpolse)
		{
		
			//Explose
			Explose();
			
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitCom, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitCom, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//InitGranade
	TimerEnabled = true;
	if(ShowDebug)
	{
		DrawDebugSphere(GetWorld(), BulletMesh->GetComponentLocation() + BulletMesh->GetForwardVector() * 10, ProjectileSetting.MaxDamageRadius, 8, FColor::Red, false, 4.0f);
		DrawDebugSphere(GetWorld(), BulletMesh->GetComponentLocation() + BulletMesh->GetForwardVector() * 10, ProjectileSetting.FalloffDamageRadius, 8, FColor::Blue, false, 4.0f);
	}
}

void AProjectileDefault_Grenade::Explose()
{
	
	
	TimerEnabled = false;
	if(ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if(ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;	
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.ExploseMaxDamage, ProjectileSetting.ExploseMaxDamage * 0.2f,
			GetActorLocation(), ProjectileSetting.MaxDamageRadius, ProjectileSetting.FalloffDamageRadius, ProjectileSetting.CoefDamageFalloff, NULL, IgnoredActor, this, nullptr);

	
	
	this->Destroy();
			
}





