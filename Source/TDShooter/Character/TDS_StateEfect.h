// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Niagara/Public/NiagaraComponent.h"
#include "TDS_StateEfect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TDSHOOTER_API UTDS_StateEfect : public UObject
{
	GENERATED_BODY()

public:
	
	virtual bool InitObject(AActor* Actor);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStakable = false;

	AActor* myActor = nullptr;
};

UCLASS()
class TDSHOOTER_API UTDS_StateEfect_ExecuteOnce : public UTDS_StateEfect
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor) override;
	virtual void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		UNiagaraSystem* NiagaraEffect = nullptr;
};
 
UCLASS()
class TDSHOOTER_API UTDS_StateEfect_ExecuteTimer : public UTDS_StateEfect
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor) override;
	virtual void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	float RateTime = 1.0f;
	
	

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	UNiagaraSystem* NiagaraEffect = nullptr;
};

UCLASS()
class TDSHOOTER_API UTDS_StateEfect_DamageOnArea : public UTDS_StateEfect
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor) override;
	virtual void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Damage OnArea")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Damage OnArea")
		float DamageRadius = 150.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Damage OnArea")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Damage OnArea")
		float RateTime = 1.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Damage OnArea")
		UNiagaraSystem* NiagaraEffect = nullptr;

	TArray<UNiagaraComponent*> EffectArr;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};
