// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSHealthComponent.h"
#include "TDSCharacterHealthComponent.generated.h"

/**
* 
*/

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldFX);


UCLASS()
class TDSHOOTER_API UTDSCharacterHealthComponent : public UTDSHealthComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
	FOnShieldChange OnShieldChange;
	FOnShieldFX OnShieldFX;
	
	FTimerHandle TimerHandle_CollDownShield;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;	
	
protected:
	float Shield = 100.0f;

	
public:
			
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ColDownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryRate = 0.1f;
	
	virtual void ChangeCurrentHealth(float ChangeValue) override;

	UFUNCTION(BlueprintCallable, Category = "Shield")
	float GetCurrentShield();
	void ChangeShieldValue(float ChangeValue);

	void ColDownShieldEnd();
	void RecoveryShield();
		
};
