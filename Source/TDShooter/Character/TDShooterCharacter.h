// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "TDSInventoryComponent.h"
#include "GameFramework/Character.h"
#include "TDShooter/FuncLibrary/Types.h"
#include "TDShooter/Items/WeaponDefault.h"
#include "TDSCharacterHealthComponent.h"
#include "TDS_IGameActor.h"

#include "TDShooterCharacter.generated.h"

UCLASS(Blueprintable)
class ATDShooterCharacter : public ACharacter, public ITDS_IGameActor
{
    GENERATED_BODY()
protected:
    virtual void BeginPlay() override;


public:
    ATDShooterCharacter();

    FTimerHandle TimerHandle_RagDollTimer;
    
    // Called every frame.
    virtual void Tick(float DeltaSeconds) override;


    virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

    /** Returns TopDownCameraComponent subobject **/
    FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
    /** Returns CameraBoom subobject **/
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
        class UTDSInventoryComponent* InventoryComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
        class UTDSCharacterHealthComponent* CharacterHealthComponent;
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ShieldFx")
        class UNiagaraSystem* ShieldFX = nullptr;

private:
    /** Top down camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
        class UCameraComponent* TopDownCameraComponent;

    /** Camera boom positioning the camera above the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
        class USpringArmComponent* CameraBoom;
   


public:

    //Cursor
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
        UMaterialInterface* CursorMaterial = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
        FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

    //Movement
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        EMovementState MovementState = EMovementState::Walk_State;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        FCharacterSpeed MovementInfo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        bool SprintRunEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        bool WalkEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        bool AimEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        bool bIsAlive = true;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
       TArray<UAnimMontage*> DeadsAnim ;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
        TSubclassOf<UTDS_StateEfect> AbilityEffect;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
        TSubclassOf<UTDS_StateEfect> AreaEffect;
    

    //Weapon
    AWeaponDefault* CurrentWeapon = nullptr;

    UDecalComponent* CurrentCursor = nullptr;

    //Effect
    TArray<UTDS_StateEfect*> Effects;

    //for demo
    /*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
    FName InitWeaponName;*/


    //FUNCTIONS Input
    //Move Function
    UFUNCTION()
    void InputAxisY(float Value);
    UFUNCTION()
    void InputAxisX(float Value);
    //Fire Function
    UFUNCTION()
        void BeginFire();
    UFUNCTION()
        void EndFire();
   
    
    float AxisX = 0.0f;
    float AxisY = 0.0f;
   

  

    //FUNCTIONS
    // Tick Func
    UFUNCTION()
        void MovementTick(float DeltaTime);
    UFUNCTION()
        void ZoomingTick(float DeltaTime);
 

    //Func
    UFUNCTION(BlueprintCallable)
        void ZoomIn();

    UFUNCTION(BlueprintCallable)
        void ZoomOut();    

    UFUNCTION(BlueprintCallable)
        void CharacterUpdate();

    UFUNCTION(BlueprintCallable)
        void ChangeMovementState();

    UFUNCTION(BlueprintCallable)
        void AttackCharEvent(bool bIsAttacked);

    UFUNCTION(BlueprintCallable)
        AWeaponDefault* GetCurrentWeapon();
    UFUNCTION(BlueprintCallable, BlueprintPure)
        int32 GetCurrentWeaponIndex();

    UFUNCTION(BlueprintCallable)
        void InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAddicionalInfo, int32 NewCurrentIndexWeapon);  
    UFUNCTION(BlueprintCallable)
        void TryReloadWeapon();

    //property
    float ZoomStep = 120.0f;
    float MaxHight = 1200.0f;
    float MinHight = 680.0f;
    float TargHight = 900.0f;

    // UFUNCTION(BlueprintCallable)
    // 	void TryReloadWeapon();
     UFUNCTION()
    	void WeaponReloadStart(UAnimMontage* Anim);
    UFUNCTION()
    	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);
     UFUNCTION(BlueprintNativeEvent)
     	void WeaponReloadStart_BP(UAnimMontage* Anim);
     UFUNCTION(BlueprintNativeEvent)
     	void WeaponReloadEnd_BP(bool bIsSuccess);


    UFUNCTION()
        void WeaponFireStart(UAnimMontage* Anim);
    UFUNCTION(BlueprintNativeEvent)
        void WeaponFireStart_BP(UAnimMontage* Anim);




    UFUNCTION(BlueprintCallable)
        UDecalComponent* GetCursorToWorld();

    //Inventory Func
    void TrySwitchNextWeapon();
    void TrySwitchPreviosWeapon();
    void DropCurrentWeapon();
    bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);


    //Inputs
    template<int32 Id>
    void TKeyPressed()
    {
        TrySwitchWeaponToIndexByKeyInput(Id);
    }
    //EndInputs

    //Ability Func
    UFUNCTION(BlueprintCallable)
    void TryAbilityEnable();
    UFUNCTION(BlueprintCallable)
        void TryAreaDamageEnable();


    UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
        int32 CurrentIndexWeapon = 0;

    //Interface
    virtual EPhysicalSurface GetSurfaceType() override;
    virtual TArray<UTDS_StateEfect*> GetAllCurentEffects() override;
    virtual void RemoveEffect(UTDS_StateEfect* RemoveEffect) override;
    virtual void AddEffect(UTDS_StateEfect* newEffect) override;

    //End Interface
    
     //
    UFUNCTION(BlueprintCallable)
    void CharDead();
    UFUNCTION(BlueprintCallable)
    void ShieldDamaged();
    void EnableRagdoll();
    virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
};
