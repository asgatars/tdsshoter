// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharacterHealthComponent.h"
#include "TDShooterCharacter.h"

void UTDSCharacterHealthComponent::ChangeCurrentHealth(float ChangeValue)
{
	//For Char
	float CurrentDamage = ChangeValue *CoefDamage;
	if (Shield > 0.0f  && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);		
	}
	else
	{
		Super::ChangeCurrentHealth(ChangeValue);
	}

	if(Shield < 0.0f)
	{
		//Todo ShieldBroken

	}
		
}

float UTDSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	if (ChangeValue < 0)
	{
		OnShieldFX.Broadcast();
	}

	Shield += ChangeValue;
	
	if(Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	if(Shield < 0.0f)
	{
		Shield = 0.0f;
	}
		

	if(GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShield, this, &UTDSCharacterHealthComponent::ColDownShieldEnd,
											ColDownShieldRecoveryTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	
	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTDSCharacterHealthComponent::ColDownShieldEnd()
{
	if(GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDSCharacterHealthComponent::RecoveryShield,
                                            ShieldRecoveryRate, true);
	}
	
}

void UTDSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoveryValue;
	if(tmp > 100.0f)
	{
		Shield = 100.0f;
		if(GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
			
		}
	}
	else
	{
		ATDShooterCharacter* myChar = Cast<ATDShooterCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
		if (myChar->bIsAlive)
		{
			Shield = tmp;
		}
		
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
	
}
