// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEfect.h"
#include "TDShooter/Interface/TDS_IGameActor.h"
#include "TDSCharacterHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "NiagaraFunctionLibrary.h"



bool UTDS_StateEfect::InitObject(AActor* Actor)
{
	myActor = Actor;
	
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UTDS_StateEfect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	myActor = nullptr;
	if(this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDS_StateEfect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	UNiagaraFunctionLibrary::SpawnSystemAttached(NiagaraEffect, Actor->GetRootComponent(), FName(), Actor->GetRootComponent()->GetComponentLocation(),
		FRotator::ZeroRotator, EAttachLocation::KeepWorldPosition, true);
	return true;
}

void UTDS_StateEfect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEfect_ExecuteOnce::ExecuteOnce()
{
	//UE_LOG(LogTemp, Warning, TEXT("TDS_StateEffect_ExecuteOnce::ExecuteOnce - Start work"));
	if(myActor)
	{
		//UE_LOG(LogTemp, Warning, TEXT("TDS_StateEffect_ExecuteOnce::ExecuteOnce - myActor: %s"), *myActor->GetName());
		UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if(myHealthComponent)
		{
			//UE_LOG(LogTemp, Error, TEXT("TDS_SateEffect_timer::ExecuteOnce was done"));
			myHealthComponent->ChangeCurrentHealth(Power);
		}
		
		
	}
	
	DestroyObject();
}

bool UTDS_StateEfect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEfect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEfect_ExecuteTimer::Execute, RateTime, true);
	

	//ToDo Effect
	
	return true;
}

void UTDS_StateEfect_ExecuteTimer::DestroyObject()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ExecuteTimer);

	Super::DestroyObject();

}

void UTDS_StateEfect_ExecuteTimer::Execute()
{
	//UE_LOG(LogTemp, Warning, TEXT("TDS_StateEffect_ExecuteTimer::Execute - Start work"));
	if(myActor)
	{
		
		//UE_LOG(LogTemp, Warning, TEXT("TDS_StateEffect_ExecuteTimer::Execute - myActor: %s"), *myActor->GetName());
		//UGameplayStatic::AplyDamege(myActor, Power, nullptr, nullptr, nullptr);
		UTDSHealthComponent* myHealthComponent = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if(myHealthComponent)
		{
			//UE_LOG(LogTemp, Error, TEXT("TDS_SateEffect_timer::Execute was done"));
			myHealthComponent->ChangeCurrentHealth(Power);
		}
		
		UNiagaraFunctionLibrary::SpawnSystemAttached(NiagaraEffect, myActor->GetRootComponent(), FName(), myActor->GetRootComponent()->GetComponentLocation(),
			FRotator::ZeroRotator, EAttachLocation::KeepWorldPosition, true);
		
	}
}

bool UTDS_StateEfect_DamageOnArea::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEfect_DamageOnArea::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEfect_DamageOnArea::Execute, RateTime, true);
	
	UNiagaraFunctionLibrary::SpawnSystemAttached(NiagaraEffect, Actor->GetRootComponent(), FName(), Actor->GetRootComponent()->GetComponentLocation(),
		FRotator::ZeroRotator, EAttachLocation::KeepWorldPosition, true);
	
	return false;
}

void UTDS_StateEfect_DamageOnArea::DestroyObject()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ExecuteTimer);
	Super::DestroyObject();
}

void UTDS_StateEfect_DamageOnArea::Execute()
{
	TArray<AActor*> IgnoredActor;
	IgnoredActor.Add(myActor);
	UGameplayStatics::ApplyRadialDamage(GetWorld(), Power, myActor->GetRootComponent()->GetComponentLocation(), DamageRadius, NULL, IgnoredActor, 
														myActor, nullptr);

	
	//Debug
	DrawDebugSphere(GetWorld(), myActor->GetRootComponent()->GetComponentLocation() + myActor->GetRootComponent()->GetForwardVector() * 10, DamageRadius,
														8, FColor::Red, false, 4.0f);
}
