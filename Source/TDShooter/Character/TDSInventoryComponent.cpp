// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventoryComponent.h"
#include "TDS_IGameActor.h"
#include "TDShooter/Game/TDSGameInctance.h"

// Sets default values for this component's properties
UTDSInventoryComponent::UTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	//Find init WeaponsSlot and First Init Weapon
	for(int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetWorld()->GetGameInstance());
		if(myGI)
		{
			if(!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo Info;
				if(myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
					WeaponSlots[i].AddicionalInfo.Round = Info.MaxRound;
				else
				{
					//WeaponSlots.RemoveAt(i);
					i--;
				}
			}
		}
	
	}
	MaxSlotsWeapon = WeaponSlots.Num();

	if(WeaponSlots.IsValidIndex(0))
	{
		if(!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AddicionalInfo, 0);
	}
	
}


// Called every frame
void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//TODO DMG Refactoring need!!!
bool UTDSInventoryComponent::SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAddicionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num() - 1)
		CorrectIndex = 0;
	else
		if (ChangeToIndex < 0)
			CorrectIndex = WeaponSlots.Num() - 1;

	FName NewIdWeapon;
	FAddicionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AddicionalInfo.Round > 0)
			{
				//good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//check ammoSlots for this weapon
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
						{
							//good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AddicionalInfo;
			}
		}
	}
	if (!bIsSuccess)
	{
		int8 iteration = 0;
		int8 Seconditeration = 0;
		int8 tmpIndex = 0;
		while (iteration < WeaponSlots.Num() && !bIsSuccess)
		{
			iteration++;

			if (bIsForward)
			{
				//Seconditeration = 0;

				tmpIndex = ChangeToIndex + iteration;
			}
			else
			{
				Seconditeration = WeaponSlots.Num() - 1;

				tmpIndex = ChangeToIndex - iteration;
			}

			if (WeaponSlots.IsValidIndex(tmpIndex))
			{
				if (!WeaponSlots[tmpIndex].NameItem.IsNone())
				{
					if (WeaponSlots[tmpIndex].AddicionalInfo.Round > 0)
					{
						//WeaponGood
						bIsSuccess = true;
						NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
						NewAdditionalInfo = WeaponSlots[tmpIndex].AddicionalInfo;
						NewCurrentIndex = tmpIndex;
					}
					else
					{
						FWeaponInfo myInfo;
						UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetWorld()->GetGameInstance());

						myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[tmpIndex].AddicionalInfo;
								NewCurrentIndex = tmpIndex;
								bIsFind = true;
							}
							j++;
						}
					}
				}
			}
			else
			{
				//go to end of LEFT of array weapon slots
				if (OldIndex != Seconditeration)
				{
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AddicionalInfo.Round > 0)
							{
								//WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
								NewAdditionalInfo = WeaponSlots[Seconditeration].AddicionalInfo;
								NewCurrentIndex = Seconditeration;
							}
							else
							{
								FWeaponInfo myInfo;
								UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0)
									{
										//WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
										NewAdditionalInfo = WeaponSlots[Seconditeration].AddicionalInfo;
										NewCurrentIndex = Seconditeration;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
				}
				else
				{
					//go to same weapon when start
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AddicionalInfo.Round > 0)
							{
								//WeaponGood, it same weapon do nothing
							}
							else
							{
								FWeaponInfo myInfo;
								UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
									{
										if (AmmoSlots[j].Count > 0)
										{
											//WeaponGood, it same weapon do nothing
										}
										else
										{
											//Not find weapon with ammo need init Pistol with infinity ammo
											UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
										}
									}
									j++;
								}
							}
						}
					}
				}
				if (bIsForward)
				{
					Seconditeration++;
				}
				else
				{
					Seconditeration--;
				}

			}
		}
	}
	if (bIsSuccess)
	{
		SetAddicionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}

	return bIsSuccess;
}

bool UTDSInventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAddicionalWeaponInfo PreviosWeaponInfo)
{
	bool bIsSucces = false;
	FName ToSwitchIdWeapon;
	FAddicionalWeaponInfo ToSwitchAdditionalInfo;

	ToSwitchIdWeapon = GetWeaponNameByIndexSlot(IndexWeaponToChange);
	ToSwitchAdditionalInfo = GetAddcionalInfoWeapon(IndexWeaponToChange);

	if (!ToSwitchIdWeapon.IsNone())
	{
		SetAddicionalInfoWeapon(PreviosIndex, PreviosWeaponInfo);
		OnSwitchWeapon.Broadcast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

		//check ammo slot for event to player
		EWeaponType ToSwitchWeaponType;
		if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
		{
			int8 AviableAmmoForWeapon = -1;
			if (CheckAmmoForWeapon(ToSwitchWeaponType, AviableAmmoForWeapon))
			{

			}
		}
		bIsSucces = true;
	}
	return bIsSucces;
}

FAddicionalWeaponInfo UTDSInventoryComponent::GetAddcionalInfoWeapon(int32 IndexWeapon)
{
	FAddicionalWeaponInfo Result;
	if(WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while(i < WeaponSlots.Num() && !bIsFind)
		{
			if(i == IndexWeapon)
			{
				Result = WeaponSlots[i].AddicionalInfo;
				bIsFind = true;

				
			}
			i++;
		}
		if(!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::GetAddicionalInfoWeapon - No Found Weapon with Index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAddicionalInfoWeapon - Not Correct Index Weapon - %d"), IndexWeapon);

	return Result;
}

int32 UTDSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 Result = -1;
	int8 i =0;
	bool bIsFind = false;
	while(i < WeaponSlots.Num() && !bIsFind)
	{
		if(WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			Result = i;
		}
		i++;		
	}
	return Result;
}

FName UTDSInventoryComponent::GetWeaponNameByIndexSlot(int32 IndexSlot)
{
	FName Result;
		
	if(WeaponSlots.IsValidIndex(IndexSlot))
	{
		Result = WeaponSlots[IndexSlot].NameItem;
	}
	
	return Result;
}

void UTDSInventoryComponent::SetAddicionalInfoWeapon(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo)
{
	if(WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if(i == IndexWeapon)
			{
				WeaponSlots[i].AddicionalInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}

		if(!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAddicionalInfoWeapon - No Found Weapon with Index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAddicionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
}

bool UTDSInventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			myGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
			WeaponType = OutInfo.WeaponType;
			bIsFind = true;
		}
	}
	return bIsFind;	
}

bool UTDSInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		myGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}
	return bIsFind;
}

void UTDSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while(i < AmmoSlots.Num() && !bIsFind)
	{
		if(AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Count += CoutChangeAmmo;
			
			if(AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
			{
				AmmoSlots[i].Count = AmmoSlots[i].MaxCount;
			}

			OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Count);
			
			bIsFind = true;
		}		
		i++;
	}
}

bool UTDSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AveableAmmoForWeapon)
{
	AveableAmmoForWeapon = 0;
	
	bool bIsFind = false;
	int8 i = 0;

	while(i < AmmoSlots.Num() && !bIsFind)
	{
		if(AmmoSlots[i].WeaponType == TypeWeapon )
		{
			bIsFind = true;
			AveableAmmoForWeapon = AmmoSlots[i].Count;
			if(AmmoSlots[i].Count > 0)
			{
				return true;
			}
		}
		i++;
	}
	
	if (AveableAmmoForWeapon <= 0)
	{
		OnWeaponAmmoEmpty.Broadcast(TypeWeapon); //visual empty ammo
	}
	else
	{
		OnWeaponAmmoAviable.Broadcast(TypeWeapon);
	}
	
	return false;
}

bool UTDSInventoryComponent::CheckCanTakenAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while(i < AmmoSlots.Num() && !result)
	{
		if(AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Count < AmmoSlots[i].MaxCount)
		{
			result = true;
		}		
		i++;
	}
	return result;
}

bool UTDSInventoryComponent::CheckCanTakenWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while(i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if(WeaponSlots[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	
	return bIsFreeSlot;
}


bool UTDSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem &DropItemInfo)
{
	bool result = false;

	if(WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponSlots[IndexSlot] = NewWeapon;
		
		SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeaponChar, -1, NewWeapon.AddicionalInfo, true);
		
		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		result = true;
	}
	return result;
}

bool UTDSInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 indexslot = -1;
	if(CheckCanTakenWeapon(indexslot))
	{
		if(WeaponSlots.IsValidIndex(indexslot))
		{
			WeaponSlots[indexslot] = NewWeapon;			
			OnUpdateWeaponSlots.Broadcast(indexslot, NewWeapon);
			return true;
		}
	}
	return false;
}

bool UTDSInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo)
{
	bool result = false;
	FName DropItemName = GetWeaponNameByIndexSlot(IndexSlot);
	
	UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetWorld()->GetGameInstance());
	if(myGI)
	{
		result = myGI->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if(WeaponSlots.IsValidIndex(IndexSlot))
		{
			DropItemInfo.DropWeaponInfo.AddicionalInfo = WeaponSlots[IndexSlot].AddicionalInfo;
			
		}		
	}
	
	return result;
}

void UTDSInventoryComponent::DropWeaponByIndex(int32 ByIndex, FDropItem& DropItemInfo)
{
	FWeaponSlot EmptyWeaponSlot;

	bool bIsCanDrop = false;
	int8 i = 0;
	int8 AviableWeaponNum = 0;
	while (i < WeaponSlots.Num() && !bIsCanDrop)
	{
		if (!WeaponSlots[i].NameItem.IsNone())
		{
			AviableWeaponNum++;
				if (AviableWeaponNum > 1)
					bIsCanDrop = true;
		}
		i++;
	}

	if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
	{
		GetDropItemInfoFromInventory(ByIndex, DropItemInfo);

		//switch weapon to valid slot weapon from start weaponslots array
		bool bIsFindWeapon = false;
		int8 j = 0;
		while (j < WeaponSlots.Num() && !bIsFindWeapon)
		{
			if (!WeaponSlots[j].NameItem.IsNone())
			{
				OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AddicionalInfo, j);
			}
			j++;
		}
	}

	WeaponSlots[ByIndex] = EmptyWeaponSlot;
	if (GetOwner()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
	{
		ITDS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
	}
	OnUpdateWeaponSlots.Broadcast(ByIndex, EmptyWeaponSlot);
}

