// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDShooterCharacter.h"

#include "ChaosStats.h"
#include "TDSInventoryComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDSInventoryComponent.h"
#include "TDShooter/Items/WeaponDefault.h"
#include "TDShooter/Game/TDSGameInctance.h"
#include "TDShooter/Game/TDShooterGameMode.h"
#include "TDShooter/Game/TDShooterPlayerController.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "ProjectileDefault.h"
#include "TDShooter/Character/TDS_StateEfect.h"
#include "NiagaraFunctionLibrary.h"


ATDShooterCharacter::ATDShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.0f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//TestFX = CreateDefaultSubobject<UNiagaraSystem>(TEXT("Niagara"));
	//TestFX->SetupAttachment(RootComponent);

	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));
	CharacterHealthComponent = CreateDefaultSubobject<UTDSCharacterHealthComponent>(TEXT("HealthComponent"));
	if(InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDShooterCharacter::InitWeapon);
	}
	if(CharacterHealthComponent)
	{
		CharacterHealthComponent->OnDead.AddDynamic(this, &ATDShooterCharacter::CharDead);
		CharacterHealthComponent->OnShieldFX.AddDynamic(this, &ATDShooterCharacter::ShieldDamaged);
	}
	
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	
	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	
	MovementTick(DeltaSeconds);

	ZoomingTick(DeltaSeconds);
}


void ATDShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName, WeaponAddicionalInfo);
	
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
	
}

void ATDShooterCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDShooterCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDShooterCharacter::InputAxisY);

	//Bind controler for Fire Event
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDShooterCharacter::BeginFire);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDShooterCharacter::EndFire);

	NewInputComponent->BindAction(TEXT("ReloadEvent"), IE_Released, this, &ATDShooterCharacter::TryReloadWeapon);
	NewInputComponent->BindAction(TEXT("DropWeapon"), IE_Released, this, &ATDShooterCharacter::DropCurrentWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDShooterCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDShooterCharacter::TrySwitchPreviosWeapon);
	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATDShooterCharacter::TryAbilityEnable);
	NewInputComponent->BindAction(TEXT("AreaEffect"), EInputEvent::IE_Pressed, this, &ATDShooterCharacter::TryAreaDamageEnable);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<9>);
	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATDShooterCharacter::TKeyPressed<0>);

}



void ATDShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDShooterCharacter::BeginFire()
{
	AttackCharEvent(true);
}

void ATDShooterCharacter::EndFire()
{
	AttackCharEvent(false);
}

void ATDShooterCharacter::TryReloadWeapon()
{
	if(CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if(CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
			CurrentWeapon->InitReload();
	}
	
}
 

void ATDShooterCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	//rotate to cursor
	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController && bIsAlive)
	{
		FVector WorldLocation;
		FVector WordlDirection;
		myController->DeprojectMousePositionToWorld(WorldLocation, WordlDirection);
		FVector CurDirection = (WordlDirection * 500000.0f) + WordlDirection;
		FVector PlaneNormal (0.0f, 0.0f, 1.0f);
		FVector Intersection;
		float T = -1.0f;
		UKismetMathLibrary::LinePlaneIntersection_OriginNormal(WorldLocation, CurDirection, GetActorLocation(), PlaneNormal, T, Intersection);
		float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Intersection).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));

		if(CurrentWeapon)
		{
			FVector Displacement = FVector::ZeroVector;
			switch (MovementState)
			{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 60.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					Displacement = FVector(0.0f, 0.0f, 60.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 20.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement =FVector(0.0f, 0.0f, 20.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Sprint_State:
					break;
				default:
					break;
			}

			CurrentWeapon->ShootEndLocation = Intersection +  Displacement;
			//aim cursor like 3d widget?
		}
	}
}



void ATDShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 500.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDShooterCharacter::ChangeMovementState()
{
	if(!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if(SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::Sprint_State;
		}
		if(WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if(WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if(!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
		
	}
	
	CharacterUpdate();

	//Weapon Update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDShooterCharacter::AttackCharEvent(bool bIsAttacked)
{
	AWeaponDefault* myWeapon =nullptr;
	myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		//To Do Chek Melee or range
		myWeapon->SetWeaponStateFire(bIsAttacked);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDShooterCharacter::AttackCharEvent - CurrentWeapon -NULL"));	
}

AWeaponDefault* ATDShooterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

int32 ATDShooterCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

//ToDo Init by id row Table
void ATDShooterCharacter::InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAddicionalInfo, int32 NewCurrentIndexWeapon)  
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	
	UTDSGameInctance* myGI = Cast<UTDSGameInctance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		 if(myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		 {
		 	if(myWeaponInfo.WeaponClass)
		 	{
		 		FVector SpawnLocation = FVector(0);
		 		FRotator SpawnRotation = FRotator(0);

		 		FActorSpawnParameters SpawnParams;
		 		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		 		SpawnParams.Owner = this;
		 		SpawnParams.Instigator = GetInstigator();

		 		AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
		 		if(myWeapon)
		 		{
		 			FAttachmentTransformRules Rule (EAttachmentRule::SnapToTarget, false);
		 			myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocetRightHand"));
		 			CurrentWeapon = myWeapon;
		 			
		 			myWeapon->WeaponSetting = myWeaponInfo;

		 			//myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;

		 			myWeapon->WeaponSetting.MagazineDrop = myWeaponInfo.MagazineDrop;
		 			myWeapon->UpdateStateWeapon(MovementState);

		 			myWeapon->WeaponInfo = WeaponAddicionalInfo;
		 			
		 			//if(InventoryComponent)
		 				CurrentIndexWeapon = NewCurrentIndexWeapon;
		 			

		 			myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDShooterCharacter::WeaponReloadStart);
		 			myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDShooterCharacter::WeaponReloadEnd);

		 			myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDShooterCharacter::WeaponFireStart);

		 			if(CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
		 				CurrentWeapon->InitReload();

		 			if(InventoryComponent)
		 				InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
		 		}
		 	}
		 }
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDShooterCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

	void ATDShooterCharacter::ZoomOut()
	{
		if (CameraBoom->TargetArmLength <= MaxHight)
		{		
			TargHight = CameraBoom->TargetArmLength + ZoomStep;	


		}
	
	}

	void ATDShooterCharacter::ZoomIn()
	{
		if (CameraBoom->TargetArmLength >= MinHight)
		{		
			TargHight = CameraBoom->TargetArmLength - ZoomStep;		
		}
	}

	void ATDShooterCharacter::ZoomingTick(float DeltaTime)
	{
		CameraBoom->TargetArmLength =  UKismetMathLibrary::FInterpTo(CameraBoom->TargetArmLength, TargHight, DeltaTime, 3.0f);
	}


void ATDShooterCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDShooterCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if(InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAddicionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATDShooterCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ATDShooterCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//in BP
}

void ATDShooterCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if(InventoryComponent)
	{
		InventoryComponent->SetAddicionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
		
	}
	
	WeaponFireStart_BP(Anim);
}

void ATDShooterCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

UDecalComponent* ATDShooterCharacter::GetCursorToWorld()
	{
		return CurrentCursor;
	}

//in one func
void ATDShooterCharacter::TrySwitchNextWeapon()
{
	if(!CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if(CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if(CurrentWeapon->WeaponReloading)
				{
					//CurrentWeapon->CancelReaload();
				}
		}

		if(InventoryComponent)
		{
			//need Timer to Switch with Aim, this method stupid i must know switch success for second logic inventory
			//now we not have success switch/ if 1 weapon switch to self
			if(InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
				
			}
		}
	}
}
//in one func
void ATDShooterCharacter::TrySwitchPreviosWeapon()
{
	if(!CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() >1)
	{
		//We have more then one weapon
		int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if(CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if(CurrentWeapon->WeaponReloading)
			{
				//CurrentWeapon->CancelReaload();
			}	
		}

		if(InventoryComponent)
		{
			//need Timer to Switch with Aim, this method stupid i must know switch success for second logic inventory
			//now we not have success switch/ if 1 weapon switch to self
			if(InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
				
			}
		}
	}
	
}

void ATDShooterCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
	}
}

bool ATDShooterCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (!CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAddicionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->WeaponInfo;
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
			}
			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
	return bIsSuccess;
}

void ATDShooterCharacter::TryAbilityEnable()
{
	if (AbilityEffect)
	{

		UTDS_StateEfect* NewEffect = NewObject<UTDS_StateEfect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this);
		}
	}
}

void ATDShooterCharacter::TryAreaDamageEnable()
{
	if (AreaEffect)
	{

		UTDS_StateEfect* NewEffect = NewObject<UTDS_StateEfect>(this, AreaEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this);
		}
	}
}


EPhysicalSurface ATDShooterCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if(CharacterHealthComponent)
	{		
		if(CharacterHealthComponent->GetCurrentShield() <= 0)
		{	
			if(GetMesh())
			{
				UMaterialInterface* myMeaterial = GetMesh()->GetMaterial(0);
				if(myMeaterial)
				{
					Result = myMeaterial->GetPhysicalMaterial()->SurfaceType;
				}		
			}
		}		
	}
	
	return Result;
}

TArray<UTDS_StateEfect*> ATDShooterCharacter::GetAllCurentEffects()
{


	return Effects;
}

void ATDShooterCharacter::RemoveEffect(UTDS_StateEfect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDShooterCharacter::AddEffect(UTDS_StateEfect* newEffect)
{
	Effects.Add(newEffect);
}

void ATDShooterCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int8 rnd = FMath::RandHelper(DeadsAnim.Num());
	if(DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}
	bIsAlive = false;
	UnPossessed();

	//Temer ragdoll
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDShooterCharacter::EnableRagdoll, TimeAnim, false);
	
	GetCursorToWorld()->SetVisibility(false);
}

void ATDShooterCharacter::ShieldDamaged()
{
	if (bIsAlive)
	{
		const FVector Location = RootComponent->GetComponentLocation();
		const FRotator Rotation = RootComponent->GetComponentRotation();
		UNiagaraFunctionLibrary::SpawnSystemAttached(ShieldFX, GetRootComponent(), FName(), Location, Rotation, EAttachLocation::KeepWorldPosition, true);
	}
}

void ATDShooterCharacter::EnableRagdoll()
{
	if(GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ATDShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if(bIsAlive)
	{
		CharacterHealthComponent->ChangeCurrentHealth(-DamageAmount);
		
	}
	if(DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if(myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());			
		}
	}
	return ActualDamage;
}



