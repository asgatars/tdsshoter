// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TDShooter/TDShooter.h"
#include "TDS_IGameActor.h"


void UTypes::AddEffectBySurfaceType(AActor*  TakeEffectActor, TSubclassOf<UTDS_StateEfect> AddEffectClass, EPhysicalSurface SurfaceType)
{

	if(SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		
			UTDS_StateEfect* myEffect = Cast<UTDS_StateEfect>(AddEffectClass->GetDefaultObject());
			if(myEffect)
			{
				bool bIsHavePossibleSurface = false;
				int8 t = 0;
				
				while (t < myEffect->PossibleInteractSurface.Num() &&  !bIsHavePossibleSurface)
				{
					if(myEffect->PossibleInteractSurface[t] == SurfaceType)
					{	
						bool bIsCanAddEffect = false;
						if (!myEffect->bIsStakable)
						{
							
							int8 i = 0;
							TArray <UTDS_StateEfect*> CurrentEffects;
							ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(TakeEffectActor);
							if (myInterface)
							{
								CurrentEffects = myInterface->GetAllCurentEffects();
							}

							if (CurrentEffects.Num() > 0)
							{
								while (i < CurrentEffects.Num() && !bIsCanAddEffect)
								{
									if (CurrentEffects[i]->GetClass() != AddEffectClass)
									{
										bIsCanAddEffect = true;
									}
									i++;
								}
							}
							else
							{
								bIsCanAddEffect = true;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
						if (bIsCanAddEffect)
						{
							bIsHavePossibleSurface = true;

							UTDS_StateEfect* NewEffect = NewObject<UTDS_StateEfect>(TakeEffectActor, AddEffectClass);
							if (NewEffect)
							{
								NewEffect->InitObject(TakeEffectActor);
							}
						}
					}
					t++;
				}
			}
	}
}

