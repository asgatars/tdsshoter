// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDShooterPlayerController.generated.h"

UCLASS()
class ATDShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATDShooterPlayerController();

protected:


	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	void PawnDead();
	virtual void OnUnPossess() override;
};


