// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInctance.h"

bool UTDSGameInctance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
    bool bIsFind = false;
    FWeaponInfo* WeaponInfoRow;
    if (WeaponInfoTable)
    {
        WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
        if(WeaponInfoRow)
        {
            bIsFind = true;
            OutInfo = *WeaponInfoRow;
        }
    }

    else
    {
        UE_LOG(LogTemp, Warning, TEXT("TDSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
    }

    return bIsFind;
}

bool UTDSGameInctance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
    bool bIsFind = false;
   
    if (DropItemInfoTable)
    {
        FDropItem* DropItemInfoRow;
        TArray<FName>RowNames = DropItemInfoTable->GetRowNames();
        
        int8 i = 0;
        while (i < RowNames.Num() && !bIsFind)
        {
            DropItemInfoRow =  DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
            if(DropItemInfoRow->DropWeaponInfo.NameItem == NameItem)
            {
                OutInfo = (*DropItemInfoRow);
                 bIsFind = true;
            }
            i++;
        }
        
    }

    else
    {
        UE_LOG(LogTemp, Warning, TEXT("TDSGameInstance::GetDropItemInfoByName - DropItemInfoTable -NULL"));
    }

    return bIsFind;
}
