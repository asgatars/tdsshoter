// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDShooterPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "TDShooter/Character/TDShooterCharacter.h"
#include "Engine/World.h"

ATDShooterPlayerController::ATDShooterPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ATDShooterPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void ATDShooterPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

}

void ATDShooterPlayerController::PawnDead()
{
	
}

void ATDShooterPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}










