// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDShooter : ModuleRules
{
	public TDShooter(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "NavigationSystem", "AIModule", "Niagara", "PhysicsCore", "Slate" });
		PublicIncludePaths.AddRange(new string[] { "TDShooter/Items", "TDShooter/Interface" });
		
	}
}
