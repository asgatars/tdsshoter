// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDShooter/Character/TDS_StateEfect.h"
#include "TDShooter/FuncLibrary/Types.h"
#include "TDS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDSHOOTER_API ITDS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	 // UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Event")
	 // 	void  AviableForEffectsBP();
	 // UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Event")
	 // 	bool  AviableForEffects();

	 virtual EPhysicalSurface GetSurfaceType();

	 virtual TArray<UTDS_StateEfect*> GetAllCurentEffects();
	 virtual void RemoveEffect(UTDS_StateEfect* RemoveEffect);
	 virtual void AddEffect(UTDS_StateEfect* newEffect);

	 //inventory
	 UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	 void DropWeaponToWorld(FDropItem DropItemInfo);
	 UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	 void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Count);
};
